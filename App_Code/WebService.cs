﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService {


    public struct MyValue
    {
        public string AccountType;
        public decimal AccountID;
    }

    public WebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

   

    [WebMethod]
    public int Multiplication(int a, int b)
    {
        return (a * b);
    }


    [WebMethod]
    public List<MyValue> Myval(decimal val)
    {
        List<MyValue> lstVal = new List<MyValue>();
        string Config = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString.ToString();
        SqlConnection objConn = new SqlConnection(Config);
        try
        {

            MyValue myval = new MyValue();

            objConn.Open();
            decimal account = Convert.ToDecimal(val);
            SqlCommand cmd = new SqlCommand("GetAccount", objConn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@AccountID", SqlDbType.Decimal));
            cmd.Parameters["@AccountID"].Value = account;
            IDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                myval.AccountID = reader.IsDBNull(0) ? -1 : reader.GetDecimal(0);
                myval.AccountType = reader.IsDBNull(1) ? null : reader.GetString(1);
                lstVal.Add(myval);
            }

            string strErrMsg = string.Empty;
        }
        catch (SqlException err)
        {
            string strErrMsg = err.Message;
        }
        catch (Exception ex)
        {
            string strErrMsg = ex.Message;
        }
        finally
        {
        }
        return lstVal;
    }
}
