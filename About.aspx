﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="About.aspx.cs" Inherits="About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Insert Values
    </h2>
    <p>
        First Name    : <asp:TextBox ID="txtNumber1" runat="server"></asp:TextBox>
        <br />
        Middle Name   :<asp:TextBox ID="txtNumber2" runat="server"></asp:TextBox>
        <br />
        LastName      :<asp:TextBox ID="txtResult" runat="server"></asp:TextBox>
        <br />
         <br />
        Email Address :<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <br />
         <br />
        Pin           :<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="btnInsert" runat = "server" Text="Insert" />
    </p>

    <script>
         //var dataString = JSON.stringify({
         //    firstname: document.getElementById("MainContent_txtNumber1").value,
         //                       middelnme: modelID,
         //                       lastname: curSuffix,
         //                       emailaddress: curPrfix,
         //                       Pin: curBaseType
         //                   });
        var dataString = JSON.stringify({
                firstname: document.getElementById("MainContent_txtNumber1").value,
                                   middelnme: document.getElementById("MainContent_txtNumber2").value,
                                   lastname: document.getElementById("MainContent_txtResult").value,
                                   emailaddress: document.getElementById("MainContent_TextBox1").value,
                                   Pin: document.getElementById("MainContent_TextBox2").value
                               });

                            $.ajax({
                                type: 'POST',
                                url: "About.aspx/InsertNames",
                                data: dataString,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                error: function (response) {
                                    var sUsrAg = navigator.userAgent;
                                    if (sUsrAg.indexOf("Firefox") > -1) {
                                        document.getElementById('lblAlertTitle').textContent = Error;
                                        document.getElementById('lblAlert').textContent = "ERROR IN JSON OBJECT : " + response.status + " " + response.d;
                                    }
                                    else {
                                        document.getElementById('lblAlertTitle').innerText = Error;
                                        document.getElementById('lblAlert').innerText = "ERROR IN JSON OBJECT : " + response.status + " " + response.d;
                                    }
                                    document.getElementById('q-alertbar-warn').style.backgroundColor = "#F00"
                                    document.getElementById('imgWarning').setAttribute('src', '../Images/alerticon_warn.png');
                                    document.getElementById('q-alertbar-warn').style.display = 'block';
                                },
                                success: function (msg) {
                                    var jsonString = msg.d;
                                }
                            });
    </script>
</asp:Content>
