﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public class UserDetails
{
    string username = string.Empty;
    string firstname = string.Empty;
    string lastname = string.Empty;
    Int64 intUserPIN;


    public Int64 PIN
    {
        get { return intUserPIN; }
        set { intUserPIN = value; }
    }

    public string UserName
    {
        get { return username; }
        set { username = value; }
    }

    public string FirstName
    {
        get { return firstname; }
        set { firstname = value; }
    }

    public string LastName
    {
        get { return lastname; }
        set { lastname = value; }
    }
}


public partial class About : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Services.WebMethod]
    private int InsertNames(string firstname, string middelnme, string lastname, string emailaddress, string Pin)
    {
        string strError = string.Empty;
        int returnval = 0;
        List<UserDetails> lstVal = new List<UserDetails>();
        string Config = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString.ToString();
        SqlConnection objConn = new SqlConnection(Config);
        try
        {

            objConn.Open();
            SqlCommand cmd = new SqlCommand("insertName", objConn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NChar, 50));
            cmd.Parameters["@FirstName"].Value = firstname;


            cmd.Parameters.Add(new SqlParameter("@MiddleName", SqlDbType.NChar, 50));
            cmd.Parameters["@MiddleName"].Value = middelnme;


            cmd.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NChar, 50));
            cmd.Parameters["@LastName"].Value = lastname;

            cmd.Parameters.Add(new SqlParameter("@EmailAddress", SqlDbType.NChar, 50));
            cmd.Parameters["@EmailAddress"].Value = lastname;


            cmd.Parameters.Add(new SqlParameter("@Pin", SqlDbType.BigInt));
            cmd.Parameters["@Pin"].Value = Pin;

            cmd.Parameters.Add("@ReturnValue", SqlDbType.Int);
            cmd.Parameters["@ReturnValue"].Direction = ParameterDirection.ReturnValue;


            cmd.ExecuteNonQuery();
            returnval = (int)cmd.Parameters["@ReturnValue"].Value;

        }
        catch (SqlException err)
        {
            string strErrMsg = err.Message;

        }
        catch (Exception ex)
        {
            string strErrMsg = ex.Message;

        }
        finally
        {
            objConn.Close();
        }
        return returnval;
    }

}
