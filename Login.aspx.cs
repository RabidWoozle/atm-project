﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        int num1;
        bool res = int.TryParse(txtPIN.Text.Trim(), out num1);
        if (res == false)
        {
            // String is not a number.
            lblFailed.Text = "Only Numeric values";
        }
        else
        {
            string Config = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString.ToString();
            SqlConnection objConn = new SqlConnection(Config);
            try
            {


                objConn.Open();
                SqlCommand cmd = new SqlCommand("[LogInQuery]", objConn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Pin", SqlDbType.BigInt));
                cmd.Parameters["@Pin"].Value = Convert.ToInt64(txtPIN.Text.Trim());

                cmd.Parameters.Add("@Count", SqlDbType.Int);
                cmd.Parameters["@Count"].Direction = ParameterDirection.ReturnValue;

                cmd.ExecuteNonQuery();
                int Check = (int)cmd.Parameters["@Count"].Value;
                if (Check == 1)
                {
                    Session["Login"] = "Session Variable";
                    Response.Redirect("Default.aspx?PIN=" + txtPIN.Text.Trim());
                }
                else
                {
                    lblFailed.Text = "Login failed. Wrong Pin number";
                }


                string strErrMsg = string.Empty;
            }
            catch (SqlException err)
            {
                string strErrMsg = err.Message;
            }
            catch (Exception ex)
            {
                string strErrMsg = ex.Message;
            }
            finally
            {
                objConn.Close();
            }
        }
    }
}