﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.Services;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Web.Script.Serialization;

public struct MyValue
{
    public string AccountType;
    public decimal AccountID;
}

public class AddressDetails{
    public int HouseNo { get; set; }
    public string StreetName { get; set; }
    public string City { get; set; }
    private string PoAddress { get; set; }
}

public class UserDetails_Default
{
    string username = string.Empty;
    string firstname = string.Empty;
    string lastname = string.Empty;
    Int64 intUserPIN;


    public Int64 PIN
    {
        get { return intUserPIN; }
        set { intUserPIN = value; }
    }

    public string UserName
    {
        get { return username; }
        set { username = value; }
    }

    public string FirstName
    {
        get { return firstname; }
        set { firstname = value; }
    }

    public string LastName
    {
        get { return lastname; }
        set { lastname = value; }
    }
}

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Connection string call
        //string Config = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString.ToString();
        //SqlConnection objConn = new SqlConnection(Config);
        //objConn.Open();
        //if (objConn.State == ConnectionState.Open)
        //{
        //    Response.Write("Yes");
        //}
        //objConn.Close();
        List<UserDetails_Default> lstDetail = GetNames();
        gvDetails.DataSource = lstDetail;
        gvDetails.DataBind();

        GetWebServiceVal(1);
        txtQueryStrin.Text = (string)Request.QueryString["PIN"];
        TextBox1.Text = (string)Session["Login"];

    }


    [System.Web.Services.WebMethod]
    public static List<MyValue> Myval(decimal val)
    {
        List<MyValue> lstVal = new List<MyValue>();
        string Config = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString.ToString();
        SqlConnection objConn = new SqlConnection(Config);
        try
        {
            
            MyValue myval = new MyValue();

            objConn.Open();
            decimal account = Convert.ToDecimal(val);
            //string x = "SELECT * FROM Users WHERE UserID='" + txtboxvalue.text + '";
            //SqlCommand cmd1 = new SqlCommand(x, objConn);
           // cmd1.CommandType = CommandType.Text;
            SqlCommand cmd = new SqlCommand("GetAccount", objConn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@AccountID", SqlDbType.BigInt));
            cmd.Parameters["@AccountID"].Value = account;
            IDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                myval.AccountID = reader.IsDBNull(0) ? -1 : reader.GetDecimal(0);
                myval.AccountType = reader.IsDBNull(1) ? null : reader.GetString(1);
                lstVal.Add(myval);
            }

            string strErrMsg = string.Empty;
        }
        catch (SqlException err)
        {
            string strErrMsg = err.Message;
        }
        catch (Exception ex)
        {
            string strErrMsg = ex.Message;
        }
        finally
        {
            objConn.Close();
        }
        return lstVal;
    }
    protected void btnMultiply_Click(object sender, EventArgs e)
    {
        WebService service = new WebService();
        List<WebService.MyValue> lstVal = new List<WebService.MyValue>();
        lstVal = service.Myval(123456);
        txtResult.Text = Convert.ToString(service.Multiplication(Convert.ToInt32(txtNumber1.Text), Convert.ToInt32(txtNumber2.Text)));
        foreach (WebService.MyValue val in lstVal)
        {
            txtNumber1.Text = Convert.ToString(val.AccountID);
            txtNumber2.Text = val.AccountType;
        }

    }
    protected void btnSerialize_Click(object sender, EventArgs e)
    {
        AddressDetails details = new AddressDetails();
        details.HouseNo = 4;
        details.StreetName = "Walton";
        details.City = "Detroit";
        Serialize(details);
    }

    public void Serialize(AddressDetails details)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(AddressDetails));
        using (TextWriter writer = new StreamWriter(@"C:\QISSI\.xml"))
        {
            serializer.Serialize(writer, details);
        }
    }

    public void DeSerialize()
    {
        XmlSerializer _xmlSerializer = new XmlSerializer(typeof(AddressDetails));
        Stream stream = new FileStream(@"C:\QISSI\Xml.xml", FileMode.Open, FileAccess.Read);
        var result = (AddressDetails)_xmlSerializer.Deserialize(stream);
        stream.Close();
    }
    protected void btnDeSerialize_Click(object sender, EventArgs e)
    {
        DeSerialize();
    }

    protected void btnJDeSerialize_Click(object sender, EventArgs e)
    {
        string json = File.ReadAllText(@"C:\QISSI\JSON.txt");

        AddressDetails details = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<AddressDetails>(json);
        string str = string.Empty;
    }

    protected void btnJSerialize_Click(object sender, EventArgs e)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();
        AddressDetails details = new AddressDetails();
        details.HouseNo = 4;
        details.StreetName = "Whereever";
        details.City = "Detroit";
        string json = ser.Serialize(details);
        File.WriteAllText(@"C:\QISSI\JSON.txt", json);
    }

    
    private List<WebService.MyValue> GetWebServiceVal(decimal account)
    {
        WebService service = new WebService();
        List<WebService.MyValue> lst = new List<WebService.MyValue>();
        lst = service.Myval(123456);
        string str = string.Empty;
        return lst;
    }
    

    private List<UserDetails_Default> GetNames()
    {
        List<UserDetails_Default> lstVal = new List<UserDetails_Default>();
        string Config = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString.ToString();
        SqlConnection objConn = new SqlConnection(Config);
        try
        {
            UserDetails_Default usr = new UserDetails_Default();
            objConn.Open();
            SqlCommand cmd = new SqlCommand("getName", objConn);
            cmd.CommandType = CommandType.StoredProcedure;


            IDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                usr.FirstName = reader.IsDBNull(0) ? "" : reader.GetString(0);
                usr.UserName = reader.IsDBNull(1) ? "" : reader.GetString(1);
                usr.LastName = reader.IsDBNull(2) ? "" : reader.GetString(2);
                usr.PIN = reader.IsDBNull(3) ? 0 : reader.GetInt64(3);
                lstVal.Add(usr);
            }

        }
        catch (SqlException err)
        {
            string strErrMsg = err.Message;
        }
        catch (Exception ex)
        {
            string strErrMsg = ex.Message;
        }
        finally
        {
            objConn.Close();
        }
        return lstVal;
    }

}
