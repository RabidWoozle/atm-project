﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h1>User PIN Query String</h1>
    <p><asp:TextBox runat="server" ID="txtQueryStrin"></asp:TextBox></p>
    <h1>PIN Session Variable</h1>
    <p><asp:TextBox runat="server" ID="TextBox1"></asp:TextBox></p>
    <h2>
        Welcome to ASP.NET!
    </h2>
    <p>
        JSON Serialization:
    </p>
    <p>
        <asp:Button ID="btn1" runat="server" onclick="btnJSerialize_Click" Text="JSON Serialize" /></p>
    <p>
        <asp:Button ID="btn2" runat="server" onclick="btnJDeSerialize_Click" Text="JSON DeSerialize" /></p>
    <br />
    <br />

    <p>
       XML Serialization:</p>
     <p>
        <asp:Button ID="btnSerialize" runat="server" onclick="btnSerialize_Click" Text="XML Serialize" /></p>
    <p>
        <asp:Button ID="btnDeSerialize" runat="server" onclick="btnDeSerialize_Click" Text="XML DeSerialize" /></p>
        <br />
        <br />
        <p>AJAX CALL</p>
        <p>
       
       <asp:TextBox ID="txtValue" runat="server"></asp:TextBox>
       <asp:Button ID="btnClick" runat = "server" OnClientClick="Onclick();return false;" Text="Ajax call" />
    </p>
    <br />
    <br />
    <p>Web service call</p>
        Number 1: <asp:TextBox ID="txtNumber1" runat="server"></asp:TextBox>
        <br />
        Number 2:<asp:TextBox ID="txtNumber2" runat="server"></asp:TextBox>
        <br />
        Ansswer    : <asp:TextBox ID="txtResult" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="btnMultiply" runat = "server" onclick="btnMultiply_Click" Text="Webservice call" />
    </p>
    <br />
   
    
        <br />
        <asp:GridView runat="server" ID="gvDetails" AutoGenerateColumns="false">
            <RowStyle BackColor="#EFF3FB" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
            <Columns>
            <asp:BoundField HeaderText="Middle Name" DataField="UserName" />
            <asp:BoundField HeaderText="First Name" DataField="FirstName" />
            <asp:BoundField HeaderText="Last Name" DataField="LastName" />
            <asp:BoundField HeaderText="PIN" DataField="PIN" />
            </Columns>
        </asp:GridView>
    <script>
        var x, y;
        function Onclick() {
            //debugger;
            if (document.getElementById("<%=txtValue.ClientID%>").value == "") {
                alert("Please enter a value to proceed");
                return false;
            }
            else {
                //val -  must match method [System.Web.Services.WebMethod]
                //public static List<MyValue> Myval(decimal val)
                
                var dataString = JSON.stringify({
                    val: document.getElementById("<%=txtValue.ClientID%>").value
                });


//                var dataString = JSON.stringify({
//                    firstname: document.getElementById("<%=txtValue.ClientID%>").value,
//                    middelnme: val1,
//                    lastname: val3,

//                });

                //firstname, string middelnme, string lastname, string emailaddres

                //to pass more parameters
//                var dataString = JSON.stringify({
                //    val: document.getElementById("<%=txtValue.ClientID%>").value,
                //    va12: document.getElementById("<%=txtValue.ClientID%>").value
//                });

                //debugger;
                //Make sure it is a number
                //if yes return true else r eturn false
                $.ajax({
                    type: 'POST',
                    url: "Default.aspx/Myval",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    error: function (response) {
                        alert("ERROR IN JSON OBJECT : " + response.status + " " + response.d);
                    },
                    success: function (msg) {
                        //If you are returning int for password check or to check successful insert or update
                        //var jsonString = JSON.stringify(msg.d);
                        //switch (jsonString) {
                        //case "0": {}
                        //case "1":{}
                        //To iterate through JSON
                        
                        $.each(msg.d, function () {
                            $.each(this, function (i, Value) {
                                if (i == "AccountType") {
                                    x = Value;

                                }
                                if (i == "AccountID") {
                                    y = Value;
                                }
                            });
                        });

                        document.getElementById("<%=txtValue.ClientID%>").value = x + y;
                    }
                });
               
            }

            return true;
        }



    </script>
</asp:Content>
